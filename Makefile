# Makefile for Open Hexagon
# Written by Ethan "flibitijibibo" Lee

# System information
UNAME = $(shell uname)
ARCH = $(shell uname -m)

# Compiler
ifeq ($(UNAME), Darwin)
	CXX = clang++
	TARGET = osx
	LIBSUFFIX = 2.dylib
else
	CXX = g++
	LIBSUFFIX = so.2
	ifeq ($(ARCH), x86_64)
		TARGET = x86_64
	else
		TARGET = x86
	endif
endif

# Executable
EXECUTABLE = $(TARGET)/openhexagon.$(TARGET)

# Compiler flags
CFLAGS = -g -std=c++11

# Libraries
DEPENDENCIES += libs/$(TARGET)/libjsoncpp.a
DEPENDENCIES += libs/$(TARGET)/libsfml-audio.$(LIBSUFFIX)
DEPENDENCIES += libs/$(TARGET)/libsfml-graphics.$(LIBSUFFIX)
DEPENDENCIES += libs/$(TARGET)/libsfml-system.$(LIBSUFFIX)
DEPENDENCIES += libs/$(TARGET)/libsfml-window.$(LIBSUFFIX)

# Parameters
INCLUDES = -Ilibs/include -IHexagon -IEntitySystem -IGameSystem

# Source directories
HEXAGONSRCDIR = Hexagon
COMPONENTSSRCDIR = Hexagon/Components
DATASRCDIR = Hexagon/Data
GLOBALSRCDIR = Hexagon/Global
UTILSSRCDIR = Hexagon/Utils
GAMESRCDIR = GameSystem
TIMELINESRCDIR = GameSystem/Timeline
ENTITYSRCDIR = EntitySystem

# Source lists
HEXAGONSRC = \
	$(HEXAGONSRCDIR)/HexagonGame.cpp \
	$(HEXAGONSRCDIR)/Main.cpp \
	$(HEXAGONSRCDIR)/MenuGame.cpp \
	$(HEXAGONSRCDIR)/PatternManager.cpp
COMPONENTSSRC = \
	$(COMPONENTSSRCDIR)/CPlayer.cpp \
	$(COMPONENTSSRCDIR)/CWall.cpp
DATASRC = \
	$(DATASRCDIR)/LevelData.cpp \
	$(DATASRCDIR)/MusicData.cpp \
	$(DATASRCDIR)/ProfileData.cpp \
	$(DATASRCDIR)/StyleData.cpp
GLOBALSRC = \
	$(GLOBALSRCDIR)/Assets.cpp \
	$(GLOBALSRCDIR)/Config.cpp \
	$(GLOBALSRCDIR)/Factory.cpp
UTILSSRC = \
	$(UTILSSRCDIR)/HSL.cpp \
	$(UTILSSRCDIR)/Utils.cpp
GAMESRC = \
	$(GAMESRCDIR)/Game.cpp \
	$(GAMESRCDIR)/GameWindow.cpp
TIMELINESRC = \
	$(TIMELINESRCDIR)/Command.cpp \
	$(TIMELINESRCDIR)/Do.cpp \
	$(TIMELINESRCDIR)/Goto.cpp \
	$(TIMELINESRCDIR)/Timeline.cpp \
	$(TIMELINESRCDIR)/Wait.cpp
ENTITYSRC = \
	$(ENTITYSRCDIR)/Component.cpp \
	$(ENTITYSRCDIR)/Entity.cpp \
	$(ENTITYSRCDIR)/Manager.cpp

# Object code lists
HEXAGONOBJ = $(HEXAGONSRC:%.cpp=%.o)
COMPONENTSOBJ = $(COMPONENTSSRC:%.cpp=%.o)
DATAOBJ = $(DATASRC:%.cpp=%.o)
GLOBALOBJ = $(GLOBALSRC:%.cpp=%.o)
UTILSOBJ = $(UTILSSRC:%.cpp=%.o)
GAMEOBJ = $(GAMESRC:%.cpp=%.o)
TIMELINEOBJ = $(TIMELINESRC:%.cpp=%.o)
ENTITYOBJ = $(ENTITYSRC:%.cpp=%.o)

# Compacted object code lists
SSVHEXAGON = $(HEXAGONOBJ) $(COMPONENTSOBJ) $(DATAOBJ) $(GLOBALOBJ) $(UTILSOBJ)
SSVSYSTEM = $(GAMEOBJ) $(TIMELINEOBJ)
SSVENTITY = $(ENTITYOBJ)

# Targets

all: hexagon system entity
	$(CXX) $(CFLAGS) $(SSVHEXAGON) $(SSVSYSTEM) $(SSVENTITY) -o $(EXECUTABLE) $(DEPENDENCIES)

hexagon: $(SSVHEXAGON)
system: $(SSVSYSTEM)
entity: $(SSVENTITY)

%.o: %.cpp
	$(CXX) $(CFLAGS) -c -o $@ $< $(INCLUDES)

clean:
	rm -f $(SSVHEXAGON) $(SSVSYSTEM) $(SSVENTITY) $(EXECUTABLE)
