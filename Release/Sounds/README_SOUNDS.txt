The sounds.json file is very simple.
Every member points to a different sound file.
Only .ogg vorbis is currently supported.

You can easily convert any music/sound file by
using the program "foobar2000", which can be
easily found on Google.