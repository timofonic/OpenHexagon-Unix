#include "Command.h"

namespace ssvs
{
	Command::~Command() { }
	void Command::initialize() { }
	void Command::update() { }
	void Command::reset() { }
} /* namespace ssvs */
